/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILETRACKER_H
#define FILETRACKER_H

#include <QObject>

class FileTracker: public QObject {
    Q_OBJECT

public:
    FileTracker();
    ~FileTracker() = default;

    Q_INVOKABLE        bool setDefaults(QString defaultDir);
    Q_INVOKABLE QStringList readAllNotes(QString folder);
    Q_INVOKABLE     QString readFirstLine(QString sourceFile);
    Q_INVOKABLE     QString readNoteFile(QString sourceFile);
    Q_INVOKABLE        bool writeNoteFile(const QString& data, QString sourceFile);
    Q_INVOKABLE        bool deleteFile(QString sourceFile);
    Q_INVOKABLE        bool fileExists(const QString sourceFile);
    Q_INVOKABLE     QString standardPaths();

signals:
    void error(const QString& msg);

Q_SIGNALS:
    void fileCreated(const QString& fileCreatedURL);

};

#endif
