/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QStandardPaths>
#include <QDirIterator>
#include <QFile>

 #include <QStandardPaths> 

#include "filetracker.h"

FileTracker::FileTracker() {

}

bool FileTracker::setDefaults(QString defaultDir) {
    QDir dir(defaultDir);
    if (!dir.exists()) {
        qDebug() << "Default directory " << defaultDir << " doesn't exists";
        return dir.mkpath(defaultDir);
    } else {
        qDebug() << "Default directory exists";
    }

    return false;
}

QStringList FileTracker::readAllNotes(QString folder) {
	// Should return a model, list? of the notes

    QStringList filters;
    QStringList notesURL;

    //Default extension is txt? Check!
    //Add a variable because extension can be changed in nextCloud Notes settings
    filters << "*.md" << "*.MD" << "*.txt" << "*.TXT";

    QDirIterator it(folder, filters, QDir::NoFilter, QDirIterator::Subdirectories);

    while (it.hasNext()) {
        QString data;
        data = it.next();

        //Adds url
        notesURL.append(data);

/*
        //Should add file name but adds url
        QFile f(data);
        f.open(QIODevice::ReadOnly);
        notesResume.append(f.fileName());
*/
    }

    //qDebug() << "notesURL = " << notesURL;
    //Check we return a list valid to use in a Model
    return notesURL;

}

QString FileTracker::readFirstLine(QString sourceFile) {
    if (sourceFile.isEmpty()) {
        emit error("Error: URL of the source file is empty");
        return QString();
    }

    QFile file(sourceFile);
    QString fileContent;

    if (file.open(QIODevice::ReadOnly)) {
        QString line;
        QTextStream t( &file );

        line = t.readLine();
        fileContent += line;

        file.close();
    } else {
        emit error("Error: Unable to open the file");
        return QString();
    }

    return fileContent;
}

//From: https://stackoverflow.com/questions/8894531/reading-a-line-from-a-txt-or-csv-file-in-qml-qt-quick
QString FileTracker::readNoteFile(QString sourceFile) {
    if (sourceFile.isEmpty()) {
        emit error("Error: URL of the source file is empty");
        return QString();
    }

    QFile file(sourceFile);
    QString fileContent;

    if (file.open(QIODevice::ReadOnly)) {
        QString line;
        QTextStream t( &file );

        do {
            line = t.readLine();
            fileContent += line + "\n";
         } while (!line.isNull());

        file.close();
    } else {
        emit error("Error: Unable to open the file");
        return QString();
    }

    return fileContent;
}

bool FileTracker::writeNoteFile(const QString& data, QString sourceFile) {

    if (sourceFile.isEmpty()) {
        qDebug() << "Error: *sourceFile* is empty";
        return false;
    }

    QFile file(sourceFile);

    //Emit a sign only if the file doesn't exist
    bool doesFileExists;

    doesFileExists = fileExists(sourceFile);

    if (!file.open(QIODevice::WriteOnly)) {
        qDebug() << "Error: cannot open sourceFile: " << sourceFile;
        return false;
    } else {
        QTextStream out(&file);
        out << data;
        file.close();

        //We need to write the data first and then emit the signal
        //to have the string for resumeLine in the model

        if (!doesFileExists) {
            emit fileCreated(sourceFile);
        }
    }

    return true;
}

bool FileTracker::deleteFile(QString sourceFile) {

    if (sourceFile.isEmpty()) {
        qDebug() << "Error: *sourceFile* is empty";
        return false;
    }

    QFile file(sourceFile);

    return file.remove();
}

bool FileTracker::fileExists(const QString sourceFile) {
    QFile file(sourceFile);

    if (file.exists()) {
        qDebug() << "sourceFile file exists: " << sourceFile;
        return true;
    } else {
        qDebug() << "sourceFile file doesn't exist: " << sourceFile;
        return false;
    }

    emit error("Error: cannot be determined if " + sourceFile + " exists");
}

QString FileTracker::standardPaths() {
    return QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
}
