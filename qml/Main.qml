/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Qt.labs.folderlistmodel 2.1

//TODO: After OTA11 this can be used
//import Qt.labs.platform 1.1
import Sync 1.0
import FileTracker 1.0

import "pages"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'memo.ubports'
    automaticOrientation: true
    anchorToKeyboard: true

    width: units.gu(45)
    height: units.gu(75)

    //Styles
    property string lightColor: "#f2c965"
    property string darkColor: UbuntuColors.jet
    property string highLightColor: "#66f2c965"
    property bool isLandscape: width > height
    property int gridmargin: units.gu(1)

    //TODO: Just for testing. booksRootFolderName should be the default folder from nextCoud notes
    property string booksRootFolderName: "test-todelete/"

    //Folders and Paths
    property string booksRootFolder: FileTracker.standardPaths() + "/" + booksRootFolderName
    property string defaultCategoryRootFolder: booksRootFolderName //+ booNkame ?

    //Signals
    signal noteAdded(int newNoteIndex) //Send new note.index when note is added to ListView


    // https://api-docs.ubports.com/sdk/apps/qml/Ubuntu.Components/AdaptivePageLayout.html
    //mainPageStack.addPageToNextColumn(sourcePage, page, properties)
    //mainPageStack.addPageToCurrentColumn(sourcePage, page, properties)
    //removePages(page)


    AdaptivePageLayout {
        id: mainPageStack
        anchors.fill: parent

        //Main panel
        primaryPageSource: MainPage {
            id: mainPage
        }

        layouts: PageColumnsLayout {
            when: width > height

            // column #0
            PageColumn {
                maximumWidth: (width * .65)
                preferredWidth: (width / 5) - units.gu(8) // -8 hack for bq E4.5
                minimumWidth: units.gu(31)
            }

            // column #1
            PageColumn {
                fillWidth: true
            }

            // column #2
            PageColumn {
                fillWidth: true
            }
        }
/*
        onColumnsChanged: {
            if (columns > 1) {
                //Push the page with the seconday AdaptivLayout
                mainPageStack.addPageToNextColumn(mainPage, secondaryStackComponent);
            }
        }

*/
    }

    Component {
        id: secondaryStackComponent

        BooksPage {
            id: secondaryPage
        }
    }

    ListModel {
        id: notes
    }

    ListModel {
        id: categories
    }

    FolderListModel {
        id: books
        rootFolder: booksRootFolder
        folder: booksRootFolder
        //nameFilters: [ "*.md", "*.MD" ]
        showHidden: true
        showDirs: true
        showFiles: false
        sortField: FolderListModel.Time
    }

    Connections {
        target: FileTracker

        onError: {
            console.log(msg);

            //TODO: Show error information to the user
        }
    }

    Component.onCompleted: {
        console.log("Create default dir '" + booksRootFolder + "'? " + FileTracker.setDefaults(booksRootFolder));
        renderNotesList(FileTracker.readAllNotes(booksRootFolder));
    }

    //Create the notes model from the list of url
    function renderNotesList(list) {
        console.log("Debug: Started renderNotesList");

        notes.clear();

        //TODO: Handle errors
        for (var i=0; i< list.length; ++i) {
            //TODO: remove booksRootFolder from url ---> list[i] = list[i].replace(booksRootFolder, "");
            //TODO: add «Book» names / color to the model (first directory under work directory) ---> var bookName = list[i].split("/")[0];
            notes.append(getFieldsForURL(list[i]));
        }
    }

    function getFieldsForURL(url) {
        var name = url.split("/")[url.split("/").length -1];
        console.log("Book might be: ", url.replace(booksRootFolder, "").split("/")[0])
        var bookName = url.replace(booksRootFolder, "").split("/")[0];
        var resumeLine = FileTracker.readFirstLine(url);

        return {
            fileURL: url,
            fileName: name,
            fileBaseName: name.replace(/(?:\.([^.]+))?$/,""),
            book: bookName == name ? "" : bookName,
            firstLine: resumeLine
        }
    }
}
