/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import "../components"

import FileTracker 1.0

Page {
    anchors.fill: parent

    header: BaseHeader {
        id: notesHeader
        title: i18n.tr("Notes")
        flickable: mainListView
        //clip: true

        extension: Sections {
            width: parent.width
            actions: [
                Action {
                    text: i18n.tr("All notes")
                    onTriggered: {
                        mainListView.model = notes
                        mainListView.delegate = notesDelegate
                    }
                },
                Action {
                    text: i18n.tr("Categories")
                    onTriggered: print("two")
                },
                Action {
                    text: i18n.tr("Books")
                    onTriggered: {
                        mainListView.delegate = booksDelegate
                        mainListView.model = books
                    }
                }
            ]

            Rectangle {
                z: -100
                anchors.fill: parent
                color: theme.palette.normal.background
            }
        }

        trailingActionBar {
            actions: Action {
                iconName: "add"
                shortcut: "Ctrl+\+"
                text: i18n.tr("New note")

                onTriggered: {
                    //CategoryURL should be the root for the current category in the vision or the one given by a Dialog
                    var categoryUrl = "";
                    //TODO: Get the name from the current folder / root
                    var name = i18n.tr("Untitled");
                    //Extension must be the same as in the nexCloud configution (.txt | .md)
                    var extension = ".md";
                    var fileToCreateOrOpen = booksRootFolder + categoryUrl + name + extension;

                    openNote(name, fileToCreateOrOpen, "", -1);

                    //Don't create a file for the not now
                    //console.log("Create a note: " + FileTracker.writeNoteFile("This is an empty note", fileToCreateOrOpen));
                }
            }
        }
    }

    ListView {
        id: mainListView
        width: parent.width
        height: units.gu(6.1)
        anchors.fill: parent

        footer: Rectangle {
            visible: notes.count > 0
            width: parent.width
            height: units.dp(1)
            color: theme.palette.normal.base
        }

        model: notes //notesModel
        delegate: notesDelegate
    }

    Loader {
        id: emptyStateLoader
        anchors.fill: mainListView
        active: notes.count === 0
        source: Qt.resolvedUrl("../components/EmptyDocument.qml")
    }

    Component {
        id: notesDelegate

        NotesListItem {}
    }

    Component {
        id: booksDelegate

        BooksListItem {}
    }

    Connections {
        target: FileTracker

        onFileCreated: {
            console.log("onFileCreated received url: " + fileCreatedURL);
            //TODO: Check this route
            var categoryUrl = "";
            var extension = ".md";
            notes.insert("0", root.getFieldsForURL(fileCreatedURL));
            root.noteAdded(0);
        }
    }

    Component.onCompleted: {
    }

    function openNote(fileBaseName, fileURL, text, index) {
        mainPageStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("NotePage.qml"), {
            "noteName": fileBaseName,
            "noteURL": fileURL,
            "noteText": text,
            "noteIndex": index
        });
    }

    //TODO: Find a better way to set a color per book
    /*
        String.prototype.hashCode = function() {
          var hash = 0, i, chr;
          if (this.length === 0) return hash;
          for (i = 0; i < this.length; i++) {
            chr   = this.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
          }
          return hash;
        };
     */
    function bookColor(bookname) {
        if (bookname == "") {
            return theme.palette.disabled.base
        }
        var colorIndex = bookname.charCodeAt(0) + bookname.charCodeAt(bookname.length -1) + bookname.charCodeAt(Math.floor(bookname.length /2))
        var contactColors = ["#DC3023", "#FF8936", "#FFB95A", "#8DB255", "#749F8D", "#48929B", "#A87CA0"];

        return contactColors[(colorIndex % contactColors.length)]
    }
}
