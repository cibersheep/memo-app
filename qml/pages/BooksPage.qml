/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

Page {
    anchors.fill: parent

    header: PageHeader {
        id: header
        title: i18n.tr('Memo')
    }

    ListView {
        id: booksList
        width: parent.width

        footer: Rectangle {
            width: parent.width
            height: units.dp(1)
            color: theme.palette.normal.base
        }

        anchors {
            fill: parent
            topMargin: header.height
        }

        model: booksModel
        delegate: bookDelegate
    }

    Component {
        id: bookDelegate

        ListItem {
            width: parent.width
            divider.visible: false
            clip: true
            //highlightColor: selectionColor

            leadingActions: ListItemActions {
                actions: Action {
                    iconName: "delete"
                    text: i18n.tr("Delete book")

                    onTriggered: {
                        //Delete book
                    }
                }
            }

            trailingActions: ListItemActions {
                actions: Action {
                    iconName: "edit"
                    text: i18n.tr("Edit book")

                    onTriggered: {
                        //Edit book info / name
                    }
                }
            }

            Rectangle {
                anchors.fill: parent
                //TODO: Colors shouldn't be random. Use fixed color for them
                color: "#" + (Math.floor(Math.random() *100)).toString() + "0"  + (Math.floor(Math.random() *100)).toString() + "0"
                opacity: .4
            }

            ListItemLayout {
                id:layout
                title.text: fileName
            }

            onClicked: {
                booksRootFolder = fileURL;
                defaultCategoryRootFolder = fileURL;
                mainPageStack.addPageToNextColumn(mainPage, secondaryStackComponent);
            }
        }
    }
}
