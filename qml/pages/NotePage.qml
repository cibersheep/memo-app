/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

import FileTracker 1.0

Page {
    id: noteEditView
    property string noteName
    property string noteURL
    property string noteText
    property int noteIndex

    anchors {
        fill: parent
        margins: units.gu(1)
    }

    //TODO: If at next column of the Adaptive Layout, header gets height from Main header+section (leaving a blank space)
    header: PageHeader {
        id: noteHeader
        title: noteName

        contents: TextField {
            id: editedNoteTitle
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width
            text: noteHeader.title

            onAccepted: {
                if (text == "") {
                    console.log("onAccepted and title was empty. Changing to default + date")
                    var dateFormat = i18n.tr("dd-MM-yyyy hh:mm:ss");
                    text = i18n.tr("Untitled-") + Qt.formatDateTime(new Date(), Qt.ISODate);
                }

                edit.focus = true;
            }
        }

        trailingActionBar {
            actions: Action {
                iconName: "ok"
                text: i18n.tr("Save note")

                onTriggered: saveNote();
            }
        }
    }

    //Check: https://api-docs.ubports.com/sdk/apps/qml/QtQuick/TextEdit.html#detailed-description
    //Check: https://api-docs.ubports.com/sdk/apps/qml/Ubuntu.Components/TextArea.html#sdk-ubuntu-components-textarea-movecursorselection
    //Or a way to show always cursor on view
    TextArea {
        id: edit
        textFormat: TextEdit.PlainText
        wrapMode: TextEdit.Wrap
        text: noteText

        anchors {
            fill: parent
            top: noteHeader.bottom
            topMargin: noteHeader.height + units.gu(1)
        }

        focus: false

        onFocusChanged: if (focus) {
            editedNoteTitle.focus = false;
            cursorPosition = text.length;
        }
    }

    //TODO: This should give focus to the title and show up the keyboard but it doesn't
    // Not even forcing the focus with editedNoteTitle.forceActiveFocus()
    Component.onCompleted: editedNoteTitle.focus = true;

    Connections {
        target: root

        onNoteAdded: {
            console.log("Received onNoteAdded: newNoteIndex=" + newNoteIndex);
            noteIndex = newNoteIndex;
        }
    }

    //To save file -> ¿edit.textDocument?
    function saveNote() {
        //Wordaround: If note doesn't unfocus / accepted after type, the last text is lost
        editedNoteTitle.focus = false;  //Sets the text
        editedNoteTitle.accepted();     //Checks if blank on onAccepted
        edit.focus = false;             //Sets the text

        //CategoryURL should be the route for the current category viewed or the one given by a Dialog
        var categoryUrl = "";

        //Extension must be the same as in the nexCloud configution (.txt | .md)
        var extension = ".md";

        //Change the baseName of the URL with the Pageheader new title id there's one
        if (editedNoteTitle.text !== noteName) {
            //Check if note with old name exists and delete it
            if (FileTracker.fileExists(noteURL)) {
                console.log(noteURL + " deleted? " + FileTracker.deleteFile(noteURL));
                if (noteIndex > -1) {
                    notes.remove(noteIndex, 1);
                } else {
                    console.log("Note is not in the ListView. noteIndex=" + noteIndex)
                }
            }

            noteURL = noteURL.replace(noteName, editedNoteTitle.text);
            noteName = editedNoteTitle.text;
            //TODO: Check if old file exists and delete after changing
        }

        //Change the subtitle of the model if we edited the note
        if (noteText != edit.text) {
            notes.set(noteIndex, { "firstLine": edit.text.split("\n")[0] });
        }

        var fileToSave = noteURL;

        console.log("Save note: " + FileTracker.writeNoteFile(edit.text, fileToSave));
    }
}
