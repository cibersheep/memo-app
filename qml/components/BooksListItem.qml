/*
* Copyright (C) 2019  Joan CiberSheep
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; version 3.
*
* ubuntu-calculator-app is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.9
import Ubuntu.Components 1.3

import FileTracker 1.0

ListItem {
    width: parent.width
    clip: true
    highlightColor: highLightColor

    leadingActions: ListItemActions {
        actions: Action {
            iconName: "delete"
            text: i18n.tr("Delete book")

            onTriggered: {
                console.log("Delete book and its content");
            }
        }
    }

    trailingActions: ListItemActions {
        actions: Action {
            iconName: "edit"
            text: i18n.tr("Edit book  name")

            onTriggered: {
                //TODO: Open Dialog?
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        color: mainPage.bookColor(fileBaseName)
    }

    ListItemLayout {
        id:layout
        title.text: fileName
        //subtitle.text:  TODO: Number of notes?
    }

    onClicked: {
        //Show note
        //console.log("Note: \n" + FileTracker.readNoteFile(fileURL));
        console.log("fileUrl = " + fileURL);
        //TODO: This should be view not and not edit
        openNote(fileBaseName, fileURL, FileTracker.readNoteFile(fileURL), index);
    }
}
